import { createApp } from "vue";
import App from "./App.vue";

// Import our custom CSS
import "./assets/scss/styles.scss";

// Import all of Bootstrap's JS
//import * as bootstrap from "bootstrap";

// Import only a subset of Bootstrap's JS
//import { Dropdown, Offcanvas, Popover } from "bootstrap";

createApp(App).mount("#app");
