# config valid only for current version of Capistrano
#lock '3.7.2'
set :application, 'portail'
set :repo_url, 'git@gricad-gitlab.univ-grenoble-alpes.fr:panays/portal.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, 'main'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/deployer/portal/'

# Default value for :scm is :git
#set :scm, :git
set :user, "deployer"
set :use_sudo, false

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'node_modules', 'public', 'dist')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# Make sure that the umask of the 'deployer' user is set to 002
# (Check /home/deployer/.profile on the server)
# Rationale: capistrano-file-permissions 1.0.0 does not set an acl mask
#            and ask to not recalculate the mask (-n option is used)
#            as a result, the user mask is used
#            If the deployer mask is set to 022, the file_permissions_paths folder will get user:www-data:rwx
#            But its acl mask will be r-x, so the folder will get an effective r-x: not writable
#set :permission_method, :acl
#set :file_permissions_users, ["www-data"]
#set :file_permissions_paths, ["app/cache"]

# Do not run composer on the server. composer has to be run locally to update the vendor/ folder
Rake::Task['deploy:updated'].prerequisites.delete('composer:install')

namespace :deploy do
  #after :restart, :clear_cache do
  #  on roles(:web), in: :groups, limit: 3, wait: 10 do
  #    # Here we can do anything such as:
  #    # within release_path do
  #    #   execute :rake, 'cache:clear'
  #    # end
  #  end
  #end

  #after 'deploy:updated', 'deploy:migrate'
  after "deploy", "vue:restart_daemons" unless ENV['NORESTART']
end
