
namespace :npm do

  desc "Install npm dependencies"
  task :dependencies do
    on roles(:app) do
      execute "cd #{release_path} && npm install"
    end
    invoke 'vue:build'
  end

  after 'deploy:updating', 'npm:dependencies'
end

namespace :vue do
  desc "Build Vue App"
  task :build do
    on roles(:app) do
      execute "cd #{release_path} && npm run build"
    end
  end

  desc "Restart Apache2 service"
  task :restart_daemons do
    on roles(:app) do
      execute "sudo service apache2 reload"
    end
  end
end
